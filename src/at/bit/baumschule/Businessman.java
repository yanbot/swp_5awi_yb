package at.bit.baumschule;

import java.util.List;

public class Businessman {
	public Businessman(String name, List<ProductionArea> productionAreas) {
		super();
		this.name = name;
		this.productionAreas = productionAreas;
	}
	private String name;
	private List<ProductionArea> productionAreas;
	
	public List<ProductionArea> getProductionAreas() {
		return productionAreas;
	}
	public void setProductionAreas(List<ProductionArea> productionAreas) {
		this.productionAreas = productionAreas;
	}
}
