package at.bit.baumschule;

public abstract class Tree {
	public Tree(float maxSize, float maxDiameter, ProductionArea productionArea, FertilizeProcess fertilizeProcess) {
		super();
		this.maxSize = maxSize;
		this.maxDiameter = maxDiameter;
		this.productionArea = productionArea;
		this.fertilizeProcess = fertilizeProcess;
		
	}

	private float maxSize;
	private float maxDiameter;
	private ProductionArea productionArea;
	private FertilizeProcess fertilizeProcess;
	
	public void fertilize() {
		fertilizeProcess.fertilize();
	}
	
	public ProductionArea getProductionArea() {
		return productionArea;
	}

	public float getMaxSize() {
		return maxSize;
	}

	public float getMaxDiameter() {
		return maxDiameter;
	}
}
