package at.bit.baumschule;

import java.util.ArrayList;
import java.util.List;

public class ProductionArea {
	public ProductionArea(String name, int size) {
		super();
		trees = new ArrayList<Tree>();
		this.name = name;
		this.size = size;
	}

	private List<Tree> trees;
	private String name;
	private int size;
	
	public List<Tree> getAllTrees(){
		return trees;
	}
	
	public void fertilizeAllTrees() {
		for (Tree tree : trees) {
			tree.fertilize();
		}
	}

	public List<Tree> getTrees() {
		return trees;
	}
	
	public void addTree(Tree tree)
	{
		trees.add(tree);
	}

	public String getName() {
		return name;
	}

	public int getSize() {
		return size;
	}
}
