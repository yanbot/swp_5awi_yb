package at.bit.baumschule;

import java.util.ArrayList;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		ProductionArea pa1 = new ProductionArea("Area 51", 100);
		ProductionArea pa2 = new ProductionArea("Area 52", 150);
		
		List<ProductionArea> listPA = new ArrayList<ProductionArea>();
		
		listPA.add(pa1);
		listPA.add(pa2);
		
		Businessman b1 = new Businessman("Jonathan", listPA);
		
		SuperGrowProcess sgp = new SuperGrowProcess();
		TopGreenProcess tgp = new TopGreenProcess();
		
		pa1.addTree(new LeafTree(5f, 0.2f, pa1, sgp));
		pa1.addTree(new Conifer(6f, 0.25f, pa1, tgp));
		pa1.addTree(new LeafTree(5.5f, 0.2f, pa1, sgp));
		
		pa1.fertilizeAllTrees();
	}

}
