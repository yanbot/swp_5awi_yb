package at.bit.cc.bowling;

import java.util.ArrayList;
import java.util.List;

public class Round {

	List<Throw> t;
	
	public Round()
	{
		t = new ArrayList<Throw>();
	}
	
	public void addThrow(Throw thro)
	{
		t.add(thro);
	}
	
	public int getScore()
	{
		int score = 0;
		
		for(int i = 0; i < t.size(); i++)
		{
			score += t.get(i).pins;
		}
		
		return score;
	}
	
	public Round duplicate()
	{
		return this;
	}
}
