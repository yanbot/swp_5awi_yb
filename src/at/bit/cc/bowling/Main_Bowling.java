package at.bit.cc.bowling;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main_Bowling {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		List<Round> rounds = new ArrayList<Round>(); 
		
		Scanner scanner = new Scanner(System.in);
		String input = scanner.nextLine();
		input = input.trim();
		
		int roundsCount = Integer.parseInt(new String(input).substring(0, new String(input).indexOf(":")));
		String[] sThrows = (new String(input).substring(new String(input).indexOf(":") + 1)).split(",");
		
		
		
		// List of Throws
		List<Throw> iThrows = new ArrayList<Throw>();
		
		for(int i = 0; i < sThrows.length; i++)
		{
			iThrows.add(new Throw(Integer.parseInt(sThrows[i])));
		}
		
		int iterations = roundsCount * 2;
		
		// List of Rounds
		for(int i = 0; i < iterations; i+=2)
		{
			rounds.add(new Round());
			if(iThrows.get(i).pins == 10)
			{
				rounds.get(rounds.size() - 1).addThrow(iThrows.get(i));
				rounds.get(rounds.size() - 1).addThrow(iThrows.get(i+1));
				rounds.get(rounds.size() - 1).addThrow(iThrows.get(i+2));
				iterations--;
				i--;
			} else
			{
				rounds.get(rounds.size() - 1).addThrow(iThrows.get(i));
				rounds.get(rounds.size() - 1).addThrow(iThrows.get(i+1));
				
				if(iThrows.get(i).pins + iThrows.get(i+1).pins == 10)
				{
					rounds.get(rounds.size() - 1).addThrow(iThrows.get(i+2));
				}
			}
		}
		
		String output = "";
		int score = 0;
		
		for(int i = 0; i < rounds.size(); i++)
		{
			score += rounds.get(i).getScore();
			output += score;
			
			if(i < rounds.size() - 1)
			{
				output +=",";
			}
		}
		
		System.out.println(output);
		
	}

}
