package at.bit.cc.bakery;

public class Unit {
	public int day;
	public int earnings;
	public int deposited;
	public boolean paid = false;
	public int count = 0;
	
	public Unit(int day, int earnings)
	{
		this.day = day;
		this.earnings = earnings;
		this.deposited = 0;
	}
	
	public Unit(Unit unit)
	{
		this.day = unit.day;
		this.earnings = unit.earnings;
		this.deposited = unit.deposited;
	}
	
	public int Deposit(int deposit) 
	{
		deposited += deposit;
		count++;
		if(deposited >= earnings)
		{
			paid = true;
			return deposited - earnings;
		}	else
		{
			return 0;
		}
	}
}
