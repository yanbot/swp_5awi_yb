package at.bit.cc.bakery;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import at.bit.cc.auction.Bid;

public class Main_Bakery {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		List<Unit> units = new ArrayList<Unit>(); 
		
		Scanner scanner = new Scanner(System.in);
		String input = scanner.nextLine();
		input = input.trim();
		
		// Split deposit and earnings
		
		String[] deposits = (new String(input).substring(new String(input).indexOf("B"))).split(" ");
		String[] earnings = (new String(input).substring(0, new String(input).indexOf("B"))).split(" ");
		
		//String[] split = input.split(" ");
		
		// Create list of units (earnings)
		
		for(int i = 0; i < earnings.length; i+=3)
		{
			units.add(new Unit(Integer.parseInt(earnings[i+1]), Integer.parseInt(earnings[i+2])));
		}
		
		// Sorting
		
		int smallestIndex = 0;
		
		for(int i = 0; i < units.size(); i++)
		{
			smallestIndex = i;
			for(int j = i; j < units.size(); j++)
			{
				if(units.get(j).day < units.get(smallestIndex).day) {
					smallestIndex = j;
				}
			}
			
			Unit temp = new Unit(units.get(i));
			units.set(i, units.get(smallestIndex));
			units.set(smallestIndex, temp);
			
		}
		
		// Add up Deposits
		
		int deposited = 0;
		
		for(int i = 0; i < deposits.length; i += 3)
		{
			for(int j = 0; j < units.size(); j++)
			{
				if(Integer.parseInt(deposits[i+1]) >= units.get(j).day && units.get(j).paid == false && units.get(j).count < 4)
				{
					deposited = units.get(j).Deposit(deposited + Integer.parseInt(deposits[i+2]));
				}
			}
		}
		

		
		
		// Testoutput
		String output = "";
		
		for(int i = 0; i < units.size(); i++)
		{
			if(units.get(i).paid == false)
			{
				output += units.get(i).day + " ";
			}
		}
		
		System.out.println(output);
		
	}

}
