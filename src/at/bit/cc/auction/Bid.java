package at.bit.cc.auction;

public class Bid {
	public String bidder;
	public int amount;
	
	public Bid(String bidder, int amount) {
		this.bidder = bidder;
		this.amount = amount;
	}

	
}
