package at.bit.cc.auction;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		List<Bid> bids = new ArrayList<Bid>(); 
		
		Scanner scanner = new Scanner(System.in);
		String input = scanner.nextLine();
		input = input.trim();
		
		String[] split = input.split(",");
		
		
		for(int i = 2; i < split.length; i += 2)
		{
			bids.add(new Bid(split[i], Integer.parseInt(split[i+1])));
		}
		
		int currentBid = Integer.parseInt(split[0]);
		int instantBuy = Integer.parseInt(split[1]);
		int highestBidder = 0;
		String history = "-," + currentBid + "," + bids.get(highestBidder).bidder + "," + currentBid;
		
		for(int i = 0; i < bids.size(); i++)
		{
			if(!bids.get(i).bidder.equals(bids.get(highestBidder).bidder))
			{
				System.out.println("Current: " + bids.get(i).bidder + " " + bids.get(i).amount);
				System.out.println("Highest: " + bids.get(highestBidder).bidder + " " + bids.get(highestBidder).amount);
				System.out.println("Amount: " + currentBid);
				if (bids.get(i).amount >= currentBid)
				{
					if(bids.get(i).amount > bids.get(highestBidder).amount)
					{
						currentBid = bids.get(highestBidder).amount + 1;
						highestBidder = i;
						history += "," + bids.get(highestBidder).bidder + "," + currentBid;
						
					} else if (bids.get(i).amount == bids.get(highestBidder).amount){
						currentBid = bids.get(i).amount;
						history += "," + bids.get(highestBidder).bidder + "," + currentBid;
					} else
					{
						currentBid = bids.get(i).amount + 1;
						history += "," + bids.get(highestBidder).bidder + "," + currentBid;
					}
					
					if(instantBuy > 0 && currentBid >= instantBuy)
					{
						highestBidder = i;
						history += "," + bids.get(highestBidder).bidder + "," + instantBuy;
						break;	
					}
				}
			} else {
				if(bids.get(i).amount > bids.get(highestBidder).amount)
				{
					highestBidder = i;
				}
				if(instantBuy > 0 && currentBid >= instantBuy)
				{
					highestBidder = i;
					history += "," + bids.get(highestBidder).bidder + "," + instantBuy;
					break;
				}
			}
				
		}
		
		System.out.println(history);
		//System.out.println(bids.get(highestBidder).bidder + "," + currentBid);
	}

}
