package at.bit.sort;

public class BubbleSort implements SortingAlgorithm {

	public int[] doSort(int[] unsortedList) {
		int[] sortedList = unsortedList;
		int n = sortedList.length;
		boolean swapped = true;
		
		do 
		{
			swapped = false;
			for(int i = 1; i < n; i++)
			{
				if( sortedList[i-1] > sortedList[i])
				{
					int temp = sortedList[i];
					sortedList[i] = sortedList[i-1];
					sortedList[i-1] = temp;
					swapped = true;
				}
			}
		} while (swapped == true);
		
		return sortedList;
	}

}
