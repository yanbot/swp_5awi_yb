package at.bit.sort;

public class InsertionSort implements SortingAlgorithm {
	
	private int[] MoveInArray(int endIndex, int startIndex, int[] array)
	{
		int temp = array[startIndex];
		for(int i = endIndex; i < endIndex; i++)
		{
			array[i] = array[i+1];
		}
		array[endIndex] = temp;
		
		return array;
	}

	public int[] doSort(int[] unsorted) {
		
		for(int i = 1; i < unsorted.length; i++)
		{
			for(int j = 0; j < i; j++)
			{
				if (unsorted[j] > unsorted[i])
				{
					unsorted = MoveInArray(i, j, unsorted);
					break;
				}
			}
		}
		
		return unsorted;
	}

}
