package at.bit.sort;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Main {
	
	public static void OutputArray(int[] array) {
		String out = "";
		out += out + array[0];
		for(int i = 1; i< array.length; i++)
		{
			out += ", " + array[i];
		}
		System.out.println(out);
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		long startTime;
		long endTime;
		
		SortingAlgorithm bubble = new BubbleSort();
		SortingAlgorithm insertion = new InsertionSort();
		SortingAlgorithm selection = new SelectionSort();
		
		
		Random rnd = new Random();
		int[] unsorted = new int[100000];
		for(int i = 0; i < 100000; i++)
		{
			unsorted[i] = rnd.nextInt();
		}
		
		
		//Bubble Sort
		startTime = System.currentTimeMillis();
		int[] sorted = bubble.doSort(unsorted);
		//OutputArray(sorted);
		endTime = System.currentTimeMillis();
		System.out.println("Bubblesort Time: " + (endTime - startTime) + "ms");
		System.out.println();
		
		
		//Insertion Sort
		startTime = System.currentTimeMillis();
		sorted = insertion.doSort(unsorted);
		//OutputArray(sorted);
		endTime = System.currentTimeMillis();
		System.out.println("Insertionsort Time: " + (endTime - startTime) + "ms");
		System.out.println();
		
		
		//Selection Sort
		startTime = System.currentTimeMillis();
		sorted = selection.doSort(unsorted);
		//OutputArray(sorted);
		endTime = System.currentTimeMillis();
		System.out.println("Selectionsort Time: " + (endTime - startTime) + "ms");
		System.out.println();
	}

}
