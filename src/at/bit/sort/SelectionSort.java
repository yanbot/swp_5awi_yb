package at.bit.sort;

public class SelectionSort implements SortingAlgorithm{
	
	public int[] doSort(int[] unsorted) {

		int smallestIndex = 0;
		
		for(int i = 0; i < unsorted.length; i++)
		{
			smallestIndex = i;
			for(int j = i; j < unsorted.length; j++)
			{
				if(unsorted[j] < unsorted[smallestIndex]) {
					smallestIndex = j;
				}
			}
			
			int temp = unsorted[i];
			unsorted[i] = unsorted[smallestIndex];
			unsorted[smallestIndex] = temp;
			
		}
		return unsorted;
	}
}
