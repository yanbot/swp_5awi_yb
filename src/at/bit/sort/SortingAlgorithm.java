package at.bit.sort;

public interface SortingAlgorithm {
	int[] doSort(int[] unsorted);
}
