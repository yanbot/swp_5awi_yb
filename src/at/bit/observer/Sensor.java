package at.bit.observer;

import java.util.ArrayList;
import java.util.List;

public class Sensor implements Observer {

	List<Observable> observers = new ArrayList<Observable>();
	
	public void addItem(Observable obs)
	{
		observers.add(obs);
	}
	
	public void informAll(String status)
	{
		for(Observable ob : observers)
		{
			ob.inform(status);
		}
	}
}
