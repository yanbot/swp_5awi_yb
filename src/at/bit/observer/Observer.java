package at.bit.observer;

public interface Observer {
	void addItem(Observable obs);
	void informAll(String status);
}
