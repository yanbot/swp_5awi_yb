package at.bit.observer;

public interface Observable {
	public void inform(String status);
}
