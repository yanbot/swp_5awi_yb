package at.bit.observer;

public class Lantern implements Observable{

	public void inform(String status) {
		System.out.println("Lantern informed. New status: " + status);
	}
	
}
