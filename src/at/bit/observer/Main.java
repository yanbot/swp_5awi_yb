package at.bit.observer;


public class Main {

	public static void main(String[] args) throws InterruptedException {
		Observer sensor = new Sensor();
		
		sensor.addItem(new Lantern());
		sensor.addItem(new ChristmasTree());

		
		sensor.informAll("Bright");
		
		Thread.sleep(1000);
		
		System.out.println();
		sensor.informAll("Dark");
	}

}
