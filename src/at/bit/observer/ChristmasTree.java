package at.bit.observer;

public class ChristmasTree implements Observable{

	public void inform(String status) {
		System.out.println("Christmastree informed. New status: " + status);
	}

}
