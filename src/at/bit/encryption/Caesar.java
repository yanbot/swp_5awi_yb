package at.bit.encryption;

public class Caesar implements Encryptor {

	@Override
	public String encrypt(String input, int offset) {
		char[] output = input.toCharArray();
		
		for(int i = 0; i < input.length(); i++)
		{
			if(output[i] >= 65 && output[i] <= 90) {
				output[i] = (char) ((output[i] + offset - 65) % 26 + 65);
				if(output[i] < 65) output[i] += 26; 
			} 
			
			else if(output[i] >= 97 && output[i] <= 122) {
				output[i] = (char) ((output[i] + offset - 97) % 26 + 97);
				if(output[i] < 97) output[i] += 26; 
			}
		}
		
		return new String(output);
	}

	@Override
	public String decrypt(String input, int offset) {
		char[] output = input.toCharArray();
		
		for(int i = 0; i < input.length(); i++)
		{
			if(output[i] >= 65 && output[i] <= 90) {
				output[i] = (char) ((output[i] - offset - 65) % 26 + 65);
				if(output[i] < 65) output[i] += 26; 
			} 
			
			else if(output[i] >= 97 && output[i] <= 122) {
				output[i] = (char) ((output[i] - offset - 97) % 26 + 97);
				if(output[i] < 97) output[i] += 26; 
			}
		}
		
		return new String(output);
	}

}
