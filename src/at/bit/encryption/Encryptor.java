package at.bit.encryption;

public interface Encryptor {
	public String encrypt(String input, int offset);
	public String decrypt(String input, int offset); 
}
