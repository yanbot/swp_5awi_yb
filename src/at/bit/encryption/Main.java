package at.bit.encryption;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Encryptor encryptor = new Caesar();
		
		String string = "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz";
		String encrypted = encryptor.encrypt(string, 3);
		String decrypted = encryptor.decrypt(encrypted, 3);
		
		System.out.println("Input: " + string);
		System.out.println("Encrypted: " + encrypted);
		System.out.println("Decrypted: " + decrypted);
	}
}
