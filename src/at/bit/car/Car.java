package at.bit.car;

public class Car {
	
	float price;
	float discount;
	float maxSpeed;
	float weight;
	
	
	Car(float price, float discount, float maxSpeed, float weight){
		this.price = price;
		this.discount = discount;
		this.maxSpeed = maxSpeed;
		this.weight = weight;
	}

	
	float getPrice() {
		return price;
	}
	
	float getPriceWithDiscount() {
		return price * (1-discount);
	}
	
	float getRealMaxSpeed() {
		if (weight < 1000) return maxSpeed * 0.9f;
		else return maxSpeed * 0.8f;
	}
}
