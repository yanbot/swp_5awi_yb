package at.bit.car;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import junit.framework.Assert;

class CarTest {

	private Car c1 = new Car(10000,10,150,400);
	
	@Test
	void testGetPrice() {

		Assert.assertEquals(10000, c1.getPrice());
	}

	@Test
	void testGetPriceWithDiscount() {
		
		Assert.assertEquals(9000, c1.getPriceWithDiscount());
	}

	@Test
	void testGetRealMaxSpeed() {
		
		Assert.assertEquals(135, c1.getRealMaxSpeed());
	}

}
