package at.bit.cds;

import java.util.ArrayList;
import java.util.List;

public class DVD {
	
	String name;
	
	List<Title> titles;
	
	public String getName()
	{
		return name;
	}
	
	public void addTitle(Title title)
	{
		titles.add(title);
	}

	public DVD(String name) {
		super();
		this.name = name;
		titles = new ArrayList<Title>();
	}
}
