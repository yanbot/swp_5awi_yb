package at.bit.cds;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Actor a1 = new Actor("John","Travolta");
		
		Song s1 = new Song("Maturaball Mix", 999999);
		Song s2 = new Song("42", 160);
		
		CD cd1 = new CD("Bestest Mix");
		
		Player p1 = new Player();
		
		
		a1.addRecord(cd1);
		cd1.addSong(s2);
		
		p1.play(s2);
		
		
		DVD dvd1 = new DVD("Shrek 4");
		
		dvd1.addTitle(new Title("Test 1", 1000));
	}

}
