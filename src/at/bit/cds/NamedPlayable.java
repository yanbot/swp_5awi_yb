package at.bit.cds;

public abstract class NamedPlayable implements Playable{
	String name;
	int length;
	
	public String getName() {
		return name;
	}
	
	public void play()
	{
		System.out.println("Now Playing \"" + name + "\" with length "+ (int)(Math.floor(length / 60)) + "m " + (length % 60) + "s");
	}
}
