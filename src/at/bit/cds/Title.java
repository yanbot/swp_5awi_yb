package at.bit.cds;

public class Title extends NamedPlayable{

	public Title(String title, int length) {
		super();
		this.name = title;
		this.length = length;
	}
}
