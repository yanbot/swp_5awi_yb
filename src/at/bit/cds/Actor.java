package at.bit.cds;

import java.util.ArrayList;
import java.util.List;

public class Actor {
	String firstName;
	String lastName;
	
	List<CD> cds;

	public Actor(String firstName, String lastName) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		
		cds = new ArrayList<CD>();
	}
	
	public List<CD> getAllRecords()
	{
		return cds;
	}
	
	public void addRecord(CD cd)
	{
		cds.add(cd);
	}
}
