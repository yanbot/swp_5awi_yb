package at.bit.cds;

import java.util.ArrayList;
import java.util.List;

public class CD {
	
	String name;
	
	List<Song> songs;
	
	public String getName()
	{
		return name;
	}
	
	public void addSong(Song song)
	{
		songs.add(song);
	}

	public CD(String name) {
		super();
		this.name = name;
		songs = new ArrayList<Song>();
	}

}
