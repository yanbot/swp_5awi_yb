package at.bit.cds;

public class Player {
	
	private String status;
	
	public Player() {
		super();
		this.status = "stopped";
	}

	public void play(Playable playable){
		status = "playing";
		playable.play();
	}
	
	public void stop() {
		status = "stopped";
	}
	
	public void pause() {
		status = "paused";
	}
}
