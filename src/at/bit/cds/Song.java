package at.bit.cds;

public class Song extends NamedPlayable{
	
	public Song(String title, int length) {
		super();
		this.name = title;
		this.length = length;
	}
}
